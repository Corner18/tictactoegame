package justForTrying;


import javafx.scene.shape.Rectangle;

import java.util.ArrayList;

public class Player {
    private Rectangle rectangle;
    private int id;
    public static  ArrayList<Player> players = new ArrayList<>();

    public Player(){


    }

    public Player(Rectangle rectangle, int id){
        this.rectangle = rectangle;
        this.id = id;
    }





    public Rectangle getRectangle() {
        return rectangle;
    }

    public void setRectangle(Rectangle rectangle) {
        this.rectangle = rectangle;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public void addPlayer(Player p){
        players.add(p);
    }


    public void setPlayers(ArrayList<Player> players) {
        this.players = players;
    }
}
