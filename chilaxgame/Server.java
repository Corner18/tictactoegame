package justForTrying;

import javafx.scene.shape.Rectangle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Server {
    private List<ClientHandler> clients;
    private int id = 1;
    private ArrayList<Player> players = new ArrayList<>();

    public Server() {
        clients = new ArrayList<ClientHandler>();
    }

    public void start(int port) {
        ServerSocket serverSocket;

        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        while (true)/*бесконечный цикл*/ {
            try {
                ClientHandler handler = new ClientHandler(serverSocket.accept(), id, players);
                Player player = new Player(new Rectangle(50, 50, 50, 50), id);
                id++;
                players.add(player);
                System.out.println("player added");
                handler.start();
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    private class ClientHandler extends Thread {
        private Socket clientSocket;
        private BufferedReader reader;
        private PrintWriter writer;
        private int id;
        private ArrayList<Player> players = new ArrayList<>();
        private String type;
        private double x;
        private double y;
        private int newId;

        public ClientHandler(Socket clientSocket, int id , ArrayList<Player> players) {
            this.clientSocket = clientSocket;
            this.players = players;
            this.id = id;
            clients.add(this);
            System.out.println("New client!");

        }

        @Override
        public void run() {
            try {
                reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                writer = new PrintWriter(clientSocket.getOutputStream(), true);

                System.out.println(id);
                writer.println(id);
                System.out.println("playerSize "+ players.size());
                Collections.reverse(players);

                for(Player player1 : players){
                    String line = "new;"+player1.getId()+";"+player1.getRectangle().getX()+";"+player1.getRectangle().getY();
                    System.out.println(line);
                    writer.println(line);
                }
                Collections.reverse(players);


                //Бесконечный цикл, который будет слушать движения каждого игрока
                while (true) {

                    String received = reader.readLine();
                    System.out.println(received);
                    String receivedData[] = received.split(";");
                    System.out.println(receivedData[0]);
                    System.out.println(receivedData[1]);
                    System.out.println(receivedData[2]);
                    System.out.println(receivedData[3]);
                /*
                    receivedData[0] : type
                    receivedData[1] : id
                    receivedData[2] : x
                    receivedData[3] : x
                */
                    type = receivedData[0];
                    newId = Integer.parseInt(receivedData[1]);
                    x = Double.parseDouble(receivedData[2]);
                    y = Double.parseDouble(receivedData[3]);
                    System.out.println(newId);
                    System.out.println(id);


                    if(type.equals("player")){
                        System.out.println(newId);
                        players.get(newId-1).getRectangle().setX(x);
                        players.get(newId-1).getRectangle().setY(y);
                        System.out.println("imfuckinghere");
                    }


                    for (ClientHandler client : clients) {
                        writer = new PrintWriter(client.clientSocket.getOutputStream(), true);
                        writer.println(received);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }


}

