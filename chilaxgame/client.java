package justForTrying;


import javafx.animation.AnimationTimer;
import javafx.animation.RotateTransition;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.transform.Translate;
import javafx.stage.Stage;
import javafx.util.Duration;


import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

public class client extends Application {
    private String message;
    private int playerId;
    private String type;
    private double x;
    private double y;
    private int id;
    private int enemyId = 0;
    private ArrayList<Player> players = new ArrayList<>();
    private ArrayList<Enemy> enemies = new ArrayList<>();
    private int score = 0;

    @Override
    public void start(Stage primaryStage) throws Exception {

        Socket socket = new Socket("127.0.0.1", 8000);
        PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
        BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        primaryStage.setTitle("ChillaxGame");
        Pane root = FXMLLoader.load(getClass().getResource("/justForTrying/chillaxGame.fxml"));


        Scene scene = new Scene(root, 600, 600);
        primaryStage.setScene(scene);

        Rectangle rectangle = new Rectangle(50, 50, 50, 50);
        rectangle.setFill(Color.CORAL);
        root.getChildren().add(rectangle);
        Label labelScore = (Label) root.lookup("#score");

        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {

                if (Math.random() < 0.02) {
                    Enemy enemy = new Enemy(new Circle(Math.random() * 600, Math.random() * 600, 15), enemyId);
                    enemy.getCircle().setFill(Color.ORCHID);
                    root.getChildren().add(enemy.getCircle());
                    enemies.add(enemy);

                    movePivot(enemy.getCircle(), 50, 50);

                    RotateTransition rt = new RotateTransition(Duration.seconds(4), enemy.getCircle());
                    rt.setToAngle(720);
                    rt.setCycleCount(Timeline.INDEFINITE);
                    rt.setAutoReverse(true);
                    rt.play();
                }


            }
        };
        timer.start();

        primaryStage.getScene().setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.LEFT) {
                rectangle.setX(rectangle.getX() - 15);
                out.println("player;" + playerId + ";" + rectangle.getX() + ";" + rectangle.getY());

                System.out.println("player;" + playerId + ";" + rectangle.getX() + ";" + rectangle.getY());
            } else if (e.getCode() == KeyCode.RIGHT) {
                rectangle.setX(rectangle.getX() + 15);
                out.println("player;" + playerId + ";" + rectangle.getX() + ";" + rectangle.getY());

            } else if (e.getCode() == KeyCode.UP) {
                rectangle.setY(rectangle.getY() - 15);
                out.println("player;" + playerId + ";" + rectangle.getX() + ";" + rectangle.getY());

            } else if (e.getCode() == KeyCode.DOWN) {
                rectangle.setY(rectangle.getY() + 15);
                out.println("player;" + playerId + ";" + rectangle.getX() + ";" + rectangle.getY());

            }

        });


        primaryStage.show();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {


                    message = in.readLine();
                    System.out.println(message);
                    playerId = Integer.parseInt(message);

                    Player player = new Player(rectangle, playerId);
                    System.out.println(player.getRectangle().getX());
                    System.out.println(player.getRectangle().getY());
                    System.out.println(playerId);
                    players.add(player);
                    out.println("new;" + playerId + ";" + rectangle.getX() + ";" + rectangle.getY());


                    while (true) {


                        message = in.readLine();
                        System.out.println(message);
                        String[] split = message.split(";");
                        type = split[0];
                        id = Integer.parseInt(split[1]);
                        x = Double.parseDouble(split[2]);
                        System.out.println(x);
                        y = Double.parseDouble(split[3]);
                        System.out.println(y);

                        if (type.equals("new") && (playerId != id)) {
                            Double innerX = x;
                            Double innerY = y;
                            int innerId = id;
                            System.out.println("playerID" + playerId);
                            Platform.runLater(new Runnable() {
                                public void run() {
                                    System.out.println("imherenew");
                                    System.out.println(players.size());

                                    Rectangle rectangle1 = new Rectangle(innerX, innerY, 50, 50);
                                    rectangle1.setFill(Color.YELLOW);
                                    Player player1 = new Player(rectangle1, innerId);
                                    root.getChildren().addAll(rectangle1);
                                    players.add(player1);
                                    type = "player";
                                }
                            });

                        }


                        if (type.equals("player") && (playerId != id)) {
                            System.out.println(playerId);
                            System.out.println("imhereplayer");
                            System.out.println(players.size());
                            for (Player player2 : players) {
                                System.out.println(player2.getId());
                                if (id == player2.getId()) {
                                    System.out.println("AAAAAAAAAAAAA");
                                    player2.getRectangle().setX(x);
                                    player2.getRectangle().setY(y);
                                }
                            }
                        }


                        for (Enemy enemy : enemies) {
                            if (player.getRectangle().getBoundsInParent().intersects(enemy.getCircle().getBoundsInParent())) {
                                Platform.runLater(new Runnable() {
                                    public void run() {
                                        score += 1;
                                        enemies.remove(enemy);
                                        root.getChildren().remove(enemy.getCircle());
                                        labelScore.setText(String.valueOf(score));


                                    }
                                });
                            }
                        }


                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();


    }

    private void movePivot(Node node, double x, double y) {
        node.getTransforms().add(new Translate(-x, -y));
        node.setTranslateX(x);
        node.setTranslateY(y);
    }


    public static void main(String[] args) {
        launch();
    }
}
