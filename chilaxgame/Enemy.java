package justForTrying;

import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

import java.util.ArrayList;

public class Enemy {
    private Circle circle;
    private int id;
    public static ArrayList<Enemy> enemies = new ArrayList<>();

    public Enemy(){


    }

    public Enemy(Circle circle, int id){
        this.circle= circle;
        this.id = id;
    }

    public Circle getCircle() {
        return circle;
    }

    public void setCircle(Circle circle) {
        this.circle = circle;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public static ArrayList<Enemy> getEnemies() {
        return enemies;
    }

    public static void setEnemies(ArrayList<Enemy> enemies) {
        Enemy.enemies = enemies;
    }
}
