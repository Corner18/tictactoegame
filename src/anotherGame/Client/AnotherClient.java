package anotherGame.Client;


import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.scene.control.Button;

import javax.jws.soap.SOAPBinding;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;


import static javafx.scene.shape.StrokeType.OUTSIDE;

public class AnotherClient extends Application {

    private PrintWriter writer;
    private BufferedReader reader;
    private String message;
    public boolean turn;

    private int[][] field = new int[3][3];
    public String XO;
    public int winner;
    private static String HOST;
    private static int PORT;


    @Override
    public void start(Stage primaryStage) throws Exception {



        Pane root = FXMLLoader.load(getClass().getResource("tictactoe.fxml"));

        primaryStage.setTitle("TicTacToe");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                field[i][j] = -1;
            }
        }

        Button r1 = (Button) root.lookup("#b1");
        Button r2 = (Button) root.lookup("#b2");
        Button r3 = (Button) root.lookup("#b3");
        Button r4 = (Button) root.lookup("#b4");
        Button r5 = (Button) root.lookup("#b5");
        Button r6 = (Button) root.lookup("#b6");
        Button r7 = (Button) root.lookup("#b7");
        Button r8 = (Button) root.lookup("#b8");
        Button r9 = (Button) root.lookup("#b9");

        System.out.println(HOST);
        System.out.println(PORT);


        Socket clientSocket = new Socket(HOST, PORT);
        writer = new PrintWriter(clientSocket.getOutputStream(), true);
        reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        r1.setOnMouseClicked(event -> {
            if (turn) {
                writer.println("0;0");
                r1.setText(XO);
                r1.setFont(Font.font(72));
                checkState(winner, r1, r2, r3, r4, r5, r6, r7, r8, r9, root);

            }
        });

        r2.setOnMouseClicked(event -> {
            if (turn) {
                writer.println("0;1");
                r2.setText(XO);
                r2.setFont(Font.font(72));
                checkState(winner, r1, r2, r3, r4, r5, r6, r7, r8, r9, root);
            }
        });

        r3.setOnMouseClicked(event -> {
            if (turn) {
                writer.println("0;2");
                r3.setText(XO);
                r3.setFont(Font.font(72));
                checkState(winner, r1, r2, r3, r4, r5, r6, r7, r8, r9, root);
            }
        });

        r4.setOnMouseClicked(event -> {
            if (turn) {
                writer.println("1;0");
                r4.setText(XO);
                r4.setFont(Font.font(72));
                checkState(winner, r1, r2, r3, r4, r5, r6, r7, r8, r9, root);
            }
        });

        r5.setOnMouseClicked(event -> {
            if (turn) {
                writer.println("1;1");
                r5.setText(XO);
                r5.setFont(Font.font(72));
                checkState(winner, r1, r2, r3, r4, r5, r6, r7, r8, r9, root);
            }
        });

        r6.setOnMouseClicked(event -> {
            if (turn) {
                writer.println("1;2");
                r6.setText(XO);
                r6.setFont(Font.font(72));
                checkState(winner, r1, r2, r3, r4, r5, r6, r7, r8, r9, root);
            }
        });

        r7.setOnMouseClicked(event -> {
            if (turn) {
                writer.println("2;0");
                r7.setText(XO);
                r7.setFont(Font.font(72));
                checkState(winner, r1, r2, r3, r4, r5, r6, r7, r8, r9, root);
            }
        });

        r8.setOnMouseClicked(event -> {
            if (turn) {
                writer.println("2;1");
                r8.setText(XO);
                r8.setFont(Font.font(72));
                checkState(winner, r1, r2, r3, r4, r5, r6, r7, r8, r9, root);
            }
        });

        r9.setOnMouseClicked(event -> {
            if (turn) {
                writer.println("2;2");
                r9.setText(XO);
                r9.setFont(Font.font(72));
                checkState(winner, r1, r2, r3, r4, r5, r6, r7, r8, r9, root);
            }
        });

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    message = reader.readLine();
                    System.out.println(message);
                    String[] split = message.split(";");
                    XO = split[0];


                    System.out.println(XO);
                    turn = Boolean.valueOf(split[1]);
                    System.out.println(turn);


                    //Бесконечный цикл. Слушаем игроков
                    while (true) {
                        message = reader.readLine();
                        System.out.println(message);
                     /*
                      Строка представляет собой:
                    message[0] :  X или O
                    message[1] : номер строки
                    message[2] : номер колонны
                    message[3] : номер комбинации, если она собрана
                     */


                        if (message != null) {
                            String[] messages = message.split(";");
                            XO = messages[0];
                            int a = Integer.parseInt(messages[1]);
                            int b = Integer.parseInt(messages[2]);
                            winner = Integer.parseInt(messages[3]);


                            if (a == 0 && b == 0) {
                                Platform.runLater(new Runnable() {
                                    public void run() {
                                        r1.setText(XO);
                                        r1.setFont(Font.font(72));
                                    }
                                });
                                checkState(winner, r1, r2, r3, r4, r5, r6, r7, r8, r9, root);
                                turn = !turn;
                            }

                            if (a == 0 && b == 1) {
                                Platform.runLater(new Runnable() {
                                    public void run() {
                                        r2.setText(XO);
                                        r2.setFont(Font.font(72));
                                        turn = !turn;
                                    }
                                });
                                checkState(winner, r1, r2, r3, r4, r5, r6, r7, r8, r9, root);
                            }

                            if (a == 0 && b == 2) {
                                Platform.runLater(new Runnable() {
                                    public void run() {
                                        r3.setText(XO);
                                        r3.setFont(Font.font(72));
                                        turn = !turn;
                                    }
                                });
                                checkState(winner, r1, r2, r3, r4, r5, r6, r7, r8, r9, root);
                            }

                            if (a == 1 && b == 0) {
                                Platform.runLater(new Runnable() {
                                    public void run() {
                                        r4.setText(XO);
                                        r4.setFont(Font.font(72));
                                        turn = !turn;
                                    }
                                });
                                checkState(winner, r1, r2, r3, r4, r5, r6, r7, r8, r9, root);
                            }

                            if (a == 1 && b == 1) {
                                Platform.runLater(new Runnable() {
                                    public void run() {
                                        r5.setText(XO);
                                        r5.setFont(Font.font(72));
                                        turn = !turn;
                                    }
                                });
                                checkState(winner, r1, r2, r3, r4, r5, r6, r7, r8, r9, root);
                            }

                            if (a == 1 && b == 2) {
                                Platform.runLater(new Runnable() {
                                    public void run() {
                                        r6.setText(XO);
                                        r6.setFont(Font.font(72));
                                        turn = !turn;
                                    }
                                });
                                checkState(winner, r1, r2, r3, r4, r5, r6, r7, r8, r9, root);
                            }

                            if (a == 2 && b == 0) {
                                Platform.runLater(new Runnable() {
                                    public void run() {
                                        r7.setText(XO);
                                        r7.setFont(Font.font(72));
                                        turn = !turn;
                                    }
                                });
                                checkState(winner, r1, r2, r3, r4, r5, r6, r7, r8, r9, root);
                            }

                            if (a == 2 && b == 1) {
                                Platform.runLater(new Runnable() {
                                    public void run() {
                                        r8.setText(XO);
                                        r8.setFont(Font.font(72));
                                        turn = !turn;
                                    }
                                });
                                checkState(winner, r1, r2, r3, r4, r5, r6, r7, r8, r9, root);
                            }

                            if (a == 2 && b == 2) {
                                Platform.runLater(new Runnable() {
                                    public void run() {
                                        r9.setText(XO);
                                        r9.setFont(Font.font(72));
                                        turn = !turn;
                                    }
                                });
                                checkState(winner, r1, r2, r3, r4, r5, r6, r7, r8, r9, root);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }


    private void checkState(int winner, Button b1, Button b2, Button b3, Button b4,
                            Button b5, Button b6, Button b7, Button b8, Button b9, Pane root) {

        switch (winner) {
            case 0:
                break;
            case 1: {
                Platform.runLater(new Runnable() {
                    public void run() {
                        b1.setText("W");
                        b2.setText("I");
                        b3.setText("N");

                        Line line = new Line();
                        line.setStartX(100);
                        line.setStartY(100);
                        line.setEndX(100);
                        line.setEndY(100);
                        line.setStrokeType(OUTSIDE);
                        Timeline timeline = new Timeline();
                        timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(1),
                                new KeyValue(line.endXProperty(), 500),
                                new KeyValue(line.endYProperty(), 100)));
                        timeline.play();
                        root.getChildren().add(line);
                    }
                });
                break;
            }

            case 2: {
                Platform.runLater(new Runnable() {
                    public void run() {

                        b4.setText("W");
                        b5.setText("I");
                        b6.setText("N");

                        Line line = new Line();
                        line.setStartX(100);
                        line.setStartY(300);
                        line.setEndX(100);
                        line.setEndY(300);
                        line.setStrokeType(OUTSIDE);
                        Timeline timeline = new Timeline();
                        timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(1),
                                new KeyValue(line.endXProperty(), 500),
                                new KeyValue(line.endYProperty(), 300)));
                        timeline.play();
                        root.getChildren().add(line);
                    }
                });
                break;
            }

            case 3: {
                Platform.runLater(new Runnable() {
                    public void run() {
                        b7.setText("W");
                        b8.setText("I");
                        b9.setText("N");
                        Line line = new Line();
                        line.setStartX(100);
                        line.setStartY(500);
                        line.setEndX(100);
                        line.setEndY(500);
                        line.setStrokeType(OUTSIDE);

                        Timeline timeline = new Timeline();
                        timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(1),
                                new KeyValue(line.endXProperty(), 500),
                                new KeyValue(line.endYProperty(), 500)));
                        timeline.play();
                        root.getChildren().add(line);
                    }
                });
                break;
            }

            case 4: {
                Platform.runLater(new Runnable() {
                    public void run() {
                        b1.setText("W");
                        b4.setText("I");
                        b7.setText("N");

                        Line line = new Line();
                        line.setStartX(100);
                        line.setStartY(100);
                        line.setEndX(100);
                        line.setEndY(100);
                        line.setStrokeType(OUTSIDE);
                        Timeline timeline = new Timeline();
                        timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(1),
                                new KeyValue(line.endXProperty(), 100),
                                new KeyValue(line.endYProperty(), 500)));
                        timeline.play();
                        root.getChildren().add(line);
                    }
                });
                break;
            }

            case 5: {
                Platform.runLater(new Runnable() {
                    public void run() {

                        b2.setText("W");
                        b5.setText("I");
                        b8.setText("N");

                        Line line = new Line();
                        line.setStartX(300);
                        line.setStartY(100);
                        line.setEndX(300);
                        line.setEndY(100);
                        line.setStrokeType(OUTSIDE);
                        Timeline timeline = new Timeline();
                        timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(1),
                                new KeyValue(line.endXProperty(), 300),
                                new KeyValue(line.endYProperty(), 500)));
                        timeline.play();
                        root.getChildren().add(line);
                    }
                });
                break;
            }

            case 6: {
                Platform.runLater(new Runnable() {
                    public void run() {

                        b3.setText("W");
                        b6.setText("I");
                        b9.setText("N");

                        Line line = new Line();
                        line.setStartX(500);
                        line.setStartY(100);
                        line.setEndX(500);
                        line.setEndY(100);
                        line.setStrokeType(OUTSIDE);
                        Timeline timeline = new Timeline();
                        timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(1),
                                new KeyValue(line.endXProperty(), 500),
                                new KeyValue(line.endYProperty(), 500)));
                        timeline.play();
                        root.getChildren().add(line);
                    }
                });
                break;
            }

            case 7: {
                Platform.runLater(new Runnable() {
                    public void run() {

                        b1.setText("W");
                        b5.setText("I");
                        b9.setText("N");

                        Line line = new Line();
                        line.setStartX(100);
                        line.setStartY(100);
                        line.setEndX(100);
                        line.setEndY(100);
                        line.setStrokeType(OUTSIDE);
                        Timeline timeline = new Timeline();
                        timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(1),
                                new KeyValue(line.endXProperty(), 500),
                                new KeyValue(line.endYProperty(), 500)));
                        timeline.play();
                        root.getChildren().add(line);
                    }
                });
                break;
            }

            case 8: {
                Platform.runLater(new Runnable() {
                    public void run() {

                        b3.setText("W");
                        b5.setText("I");
                        b7.setText("N");

                        Line line = new Line();
                        line.setStartX(500);
                        line.setStartY(100);
                        line.setEndX(500);
                        line.setEndY(100);
                        line.setStrokeType(OUTSIDE);
                        Timeline timeline = new Timeline();
                        timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(1),
                                new KeyValue(line.endXProperty(), 100),
                                new KeyValue(line.endYProperty(), 500)));
                        timeline.play();
                        root.getChildren().add(line);
                    }
                });
                break;
            }

        }
    }

    public static void main(String[] args) {
        HOST = args[0];
        PORT = Integer.parseInt(args[1]);
        launch();
    }
}
