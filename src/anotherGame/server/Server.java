package anotherGame.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server {
    private int field[][] = new int[3][3];
    private int amount = 1; // количество ходов
    private List<ClientHandler> clients;

    public Server() {
        clients = new ArrayList<ClientHandler>();
    }

    public void start(int port) {
        ServerSocket serverSocket;

        try { // заполняем все поле отрицательными числами
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    field[i][j] = -1;
                }
            }

            serverSocket = new ServerSocket(port);


        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        while (true)/*бесконечный цикл*/ {
            try {
                int xo = amount % 2 == 0 ? 1 : 0;
                amount++;
                ClientHandler handler = new ClientHandler(serverSocket.accept(), xo, field);
                handler.start();

            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    private class ClientHandler extends Thread {
        private Socket clientSocket;
        private BufferedReader reader;
        private PrintWriter writer;
        private int xo; //переменная показывающая каким символом сейчас играют
        private int field[][]; // игровое поле
        private boolean turn;


        public ClientHandler(Socket clientSocket, int xo, int[][] field) {
            this.clientSocket = clientSocket;
            this.xo = xo;
            this.field = field;
            clients.add(this);
            System.out.println("New client!");

        }


        @Override
        public void run() {
            try {
                reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                writer = new PrintWriter(clientSocket.getOutputStream(), true);
                turn = (xo == 1);
                String message = (turn ? "X;" : "O;") + turn;
                writer.println(message);

                //Бесконечный цикл, который будет слушать движения каждого игрока
                // Каждый раз, когда игрок ставит X или O, ход приходит сюда и раздается другому игроку
                while (true) {
                    //считываем данные при нажатии кнопки
                    String received = reader.readLine();
                    System.out.println(received);
                    String receivedData[] = received.split(";");
                /*
                    receivedData[0] : номер ряда
                    receivedData[1] : номер колонны
                */
                    int a = Integer.parseInt(receivedData[0]);
                    int b = Integer.parseInt(receivedData[1]);
                /*
                    X : 1
                    O : 0
                */
                    field[a][b] = xo;
                    System.out.println("field" + a + b + "=" + field[a][b]);

                /*
                Формируем сообщение. символ + номер ряда + номер колонны + номер комбинации
                */

                    String chain = "";
                    chain += ((xo == 1) ? "X;" : "O;") + a + ";" + b + ";";
                    int winner = win(xo);
                    chain+=winner;
                    for (ClientHandler client : clients) {
                        writer = new PrintWriter(client.clientSocket.getOutputStream(), true);
                        writer.println(chain);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //Функция проверяет, выиграл ли кто-либо из игроков
        public int win(int xo) {
            if (field[0][0] == xo && field[0][1] == xo && field[0][2] == xo) {
                return 1;
            }
            else if (field[1][0] == xo && field[1][1] == xo && field[1][2] == xo) {
                return 2;
            }
            else if (field[2][0] == xo && field[2][1] == xo && field[2][2] == xo) {
                return 3;
            }
            else if (field[0][0] == xo && field[1][0] == xo && field[2][0] == xo) {
                return 4;
            }
            else if (field[0][1] == xo && field[1][1] == xo && field[2][1] == xo) {
                return 5;
            }
            else if (field[0][2] == xo && field[1][2] == xo && field[2][2] == xo) {
                return 6;
            }
            else if (field[0][0] == xo && field[1][1] == xo && field[2][2] == xo) {
                return 7;
            }
            else if (field[0][2] == xo && field[1][1] == xo && field[2][0] == xo) {
                return 8;
            }
            else return 0;

        }

    }


}













